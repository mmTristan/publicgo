package public

import (
	"fmt"
)

//Hello prints hello and returns an int of 25
func Hello() int {
	fmt.Println("hello")
	fmt.Println("hello from line 2 at v0.1.1")
	return 25
}
